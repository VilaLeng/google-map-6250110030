import 'package:flutter/material.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatefulWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  _MyAppState createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {
  late GoogleMapController mapController;

  final LatLng _center = const LatLng(7.531006324610513, 99.57653859838703);

  Future<void> _onMapCreated(GoogleMapController controller) async{
    mapController = controller;
    String value = await DefaultAssetBundle.of(context)
            .loadString('assets/mapstyle.json');
    mapController.setMapStyle(value);
  }
  Set<Marker> _createMarker() {
    return {
      Marker(
        markerId: MarkerId("marker_1"),
        position: _center,
        infoWindow: InfoWindow(title: 'Home Pongpicha', snippet: 'Pongpicha Dormitory, Khaun Pring Phanich'),
       // rotation: 90,
        icon: BitmapDescriptor.defaultMarker,
      ),
      Marker(
        markerId: MarkerId("marker_2"),
        position: LatLng(7.518815206943336, 99.57875057502228),
        infoWindow: InfoWindow(title: 'PSU TRANG', snippet: 'Prince of Songkla University, Trang Campus'),
        rotation: 90,
        icon: BitmapDescriptor.defaultMarkerWithHue(BitmapDescriptor.hueCyan),
      ),
    };
  }
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: Scaffold(
        appBar: AppBar(
          title: const Text('6250110030 Google Maps'),
          backgroundColor: Colors.green[700],
        ),
        body: GoogleMap(
          myLocationEnabled: true,
          mapToolbarEnabled: true,
          //mapType: MapType.satellite,
          onMapCreated: _onMapCreated,
          initialCameraPosition: CameraPosition(
            target: _center,
            zoom: 15.0,
          ),
          markers: _createMarker(),
        ),
      ),
    );
  }
}
